using Android.Content;
using MvvmCross.Droid.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform;
using SafeTrip.Droid.Maps;
using SafeTrip.Core.Interfaces;
using SafeTrip.Core.Database;
using SafeTrip.Droid.Database;
using SafeTrip.Droid.Services;

namespace SafeTrip.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new SafeTrip.Core.App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override void InitializeFirstChance()
        {
            Mvx.LazyConstructAndRegisterSingleton<IGeoCoder, GeoCoder>();
            Mvx.LazyConstructAndRegisterSingleton<ISqlite, SqliteDroid>();
            Mvx.LazyConstructAndRegisterSingleton<IDialogService, DialogService>();
            Mvx.LazyConstructAndRegisterSingleton<IVehicleDatabase, VehicleDatabase>();
            base.InitializeFirstChance();
        }
    }
}
