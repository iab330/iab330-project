using Android.App;
using Android.OS;
using Android.Content;
using MvvmCross.Droid.Views;

namespace SafeTrip.Droid.Views
{
    [Activity(Label = "View for ReportViewModel")]
    public class ReportView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ReportView);
        }
        public void CallEmergency(string phoneNumber)
        {
            var number = Android.Net.Uri.Parse(phoneNumber);
            var intent = new Intent(Intent.ActionDial, number);
            StartActivity(intent);
        }
    }
}
