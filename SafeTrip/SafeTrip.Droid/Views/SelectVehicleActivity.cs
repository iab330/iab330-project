using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using MvvmCross.Droid.Views;
using SafeTrip.Core.ViewModels;

namespace SafeTrip.Droid.Views
{
    [Activity(Label = "@string/selectVehicleActivity")]
    public class SelectVehicleActivity : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.SelectVehicleView);

            //Custom (centered text) Actionbar
            ActionBar.SetDisplayShowCustomEnabled(true);
            ActionBar.SetCustomView(Resource.Layout.centeredActionbar);
            TextView titleText = (TextView)FindViewById(Resource.Id.titleText);
            titleText.SetText(Resource.String.selectVehicleActivity);
        }

        protected override void OnResume()
        {
            var vm = (SelectVehicleViewModel)ViewModel;
            vm.OnResume();
            base.OnResume();
        }
    }
}
