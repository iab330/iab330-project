using Android.App;
using Android.OS;
using Android.Widget;
using MvvmCross.Droid.Views;

namespace SafeTrip.Droid.Views
{
    [Activity(Label = "@string/addVehicleActivity")]
    public class AddVehicleActivity : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.AddVehicle);

            //Custom (centered text) Actionbar
            ActionBar.SetDisplayShowCustomEnabled(true);
            ActionBar.SetCustomView(Resource.Layout.centeredActionbar);
            TextView titleText = (TextView)FindViewById(Resource.Id.titleText);
            titleText.SetText(Resource.String.addVehicleActivity);
        }

        public void BlankText()
        {

        }
    }
}
