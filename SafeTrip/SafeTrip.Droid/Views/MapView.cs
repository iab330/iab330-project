using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Locations;
using Android.Content;
using Android.Widget;
using System;
using SafeTrip.Core.ViewModels;
using SafeTrip.Core.Models;
using Android.Content.Res;
using System.IO;
using System.Collections.Generic;
using EstimoteSdk;
using EstimoteSdk.EddystoneSdk;
using System.Collections.ObjectModel;
using System.Linq;

namespace SafeTrip.Droid.Views
{
    [Activity(Label = "@string/ApplicationName")]
    public class MapView : MvxActivity, IOnMapReadyCallback, BeaconManager.IServiceReadyCallback
    {
        double userLat = -27.4;
        double userLong = 153;
        Dictionary<Bridge, Marker> markerSets;

        private delegate IOnMapReadyCallback OnMapReadyCallback();
        private GoogleMap mMap;
        MapViewModel mapVm;

        BeaconManager beaconManager;
        string edScanId;
        bool isScanning;

        public void OnServiceReady()
        {
            isScanning = true;
            edScanId = beaconManager.StartEddystoneScanning();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.MapView);
            mapVm = ViewModel as MapViewModel;
	        mapVm.Bridges.ReadAndSetupBridges(ReadContent());
            //__OPTIMIZATION__: we are storing bridge twice (here and the total list of bridges in mapviewmodel)
            markerSets = new Dictionary<Bridge, Marker>();
            SetUpMap();

            beaconManager = new BeaconManager(this);
            beaconManager.Eddystone += BeaconManager_Eddystone;
            beaconManager.Connect(this);

            //Custom (centered text) Actionbar
            ActionBar.SetDisplayShowCustomEnabled(true);
            ActionBar.SetCustomView(Resource.Layout.centeredActionbar);
            TextView titleText = (TextView)FindViewById(Resource.Id.titleText);
            titleText.SetText(Resource.String.ApplicationName);
        }

        private void BeaconManager_Eddystone(object sender, BeaconManager.EddystoneEventArgs e)
        {
            mapVm.EddyStoneList.Clear();
            var eddys = new List<Eddystone>(e.Eddystones);
            var sortedEddys = eddys.OrderByDescending(ed => ed.Rssi);
            foreach (var stone in sortedEddys)
            {
                mapVm.EddyStoneList.Add(new Core.Models.EddyStone
                {
                    CalibratedTxPower = stone.CalibratedTxPower,
                    Instance = stone.Instance,
                    MacAddress = stone.MacAddress.ToString(),
                    Namespace = stone.Namespace,
                    Rssi = stone.Rssi,
                    TelemetryLastSeenMillis = Convert.ToInt16(stone.TelemetryLastSeenMillis),
                    Url = stone.Url
                });
            }

            mapVm.BeaconInRange();
        }

        protected override void OnStop()
        {
            base.OnStop();
            if (!isScanning)
            {
                return;
            }
            beaconManager.StopEddystoneScanning(edScanId);
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            beaconManager.Disconnect();
        }

        //Takes an index of the list of bridges and sets the lat/long as well as
        //the "height" (in the title)
        private MarkerOptions SetMarkerDetails(int index)
		{
            MarkerOptions markerAtIndex = new MarkerOptions();
			Bridge bridgeAtIndex = mapVm.Bridges.BridgeList[index];

			markerAtIndex.SetPosition(new LatLng(bridgeAtIndex.latitude, bridgeAtIndex.longitude));
			markerAtIndex.SetTitle("Height: " + bridgeAtIndex.Signed_Clearance +"m");

            return markerAtIndex;
		}

        //Pulls the JSON file from assets and return as string
		private string ReadContent()
		{
			AssetManager assets = Assets;
			using (StreamReader sr = new StreamReader(assets.Open("lowBridge.json")))
			{
				return sr.ReadToEnd() as string;
            }
		}
        
        private void SetUpMap()
        {
            if (mMap == null)
            {
                FragmentManager.FindFragmentById<MapFragment>(Resource.Id.map).GetMapAsync(this);
            }

        }

        private LatLng GetCurrentLocation()
        {
            var service = (LocationManager)GetSystemService(LocationService);
            var criteria = new Criteria();
            var provider = service.GetBestProvider(criteria, false);
            var location = service.GetLastKnownLocation(provider);
            return (location != null) ? new LatLng(location.Latitude, location.Longitude) : new LatLng(50, 3);
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            mapVm.OnMapSetup(MoveToLocation);
            mMap = googleMap;
            mMap.MyLocationEnabled = true;
            mMap.MyLocationChange += Map_MyLocationChange;
            //mMap.MapLongClick += Map_MapClick;

            if (mMap != null)
            {
                MarkAllBridges();
            }

        }

        //Drops all pins from the BridgeList array onto our gmap
        private void MarkAllBridges()
        {
            for (int i = 0; i < mapVm.Bridges.BridgeList.Count; i++)
            {
                Marker gmapPin = mMap.AddMarker(SetMarkerDetails(i));
                if (mapVm.CurrentVehicle != null)
                {
                    if (mapVm.CurrentVehicle.CompareCanPass(mapVm.Bridges.BridgeList[i].Signed_Clearance))
                    {
                        if (gmapPin.Visible == true)
                        {
                            gmapPin.Visible = false;
                        }
                    }
                    else
                    {
                        if (gmapPin.Visible == false)
                        {
                            gmapPin.Visible = true;
                        }
                    }
                }
                markerSets.Add(mapVm.Bridges.BridgeList[i], gmapPin);
            }
        }

        private void UpdateAllBridges()
        {
            foreach (KeyValuePair<Bridge, Marker> entry in markerSets)
            {
                if (mapVm.CurrentVehicle.CompareCanPass(entry.Key.Signed_Clearance))
                {
                    if (entry.Value.Visible == true)
                    {
                        entry.Value.Visible = false;
                    }
                }
                else
                {
                    if (entry.Value.Visible == false)
                    {
                        entry.Value.Visible = true;
                    }
                }
            }
        }

        private void Map_MyLocationChange(object sender, GoogleMap.MyLocationChangeEventArgs e)
        {
            mMap.MyLocationChange -= Map_MyLocationChange;
            var location = new GeoLocation(e.Location.Latitude, e.Location.Longitude, e.Location.Altitude);

            userLat = e.Location.Latitude;
            userLong = e.Location.Longitude;

            MoveToLocation(location);
            mapVm.OnMyLocationChanged(location);

            FindNearestBridge();

            //BridgeRadiusTest();
        }

        //Finds the closest bridge to the users current lat/long
        private void FindNearestBridge()
        {
            List<Bridge> relevant = new List<Bridge>();
            foreach (KeyValuePair<Bridge,Marker> entry in markerSets)
            {
                if (entry.Value.Visible == true)
                {
                    relevant.Add(entry.Key);
                } 
            }
            Log.Info("myapp", relevant.Count.ToString());
            Tuple<Bridge, double> nearBridge = mapVm.Bridges.GetNearest(userLat, userLong, relevant);
			if (nearBridge != null)
			{
				Log.Info("myapp", nearBridge.Item1.ASSET_ID + " " + nearBridge.Item2);
				MakeToast(nearBridge.Item1.Street_Name, nearBridge.Item2);
			}
        }

        //Creates a toast with the location of the closest bridge and warns user
        //exactly how close they are
        private void MakeToast(string location, double distanceToPin)
        {
            Vibrator vibrator = (Vibrator)GetSystemService(Context.VibratorService);
            if (distanceToPin <= 50)
            {
                Toast.MakeText(this, "DANGER: Low Bridge within 50m!", ToastLength.Long).Show();
                Toast.MakeText(this, "DANGER: Low Bridge on " + location, ToastLength.Long).Show();
            }
            else if (distanceToPin <= 100 && distanceToPin > 50)
            {
                Toast.MakeText(this, "WARNING: Low Bridge within 100m", ToastLength.Long).Show();
                Toast.MakeText(this, "WARNING: Low Bridge on " + location, ToastLength.Long).Show();
            }
            else if (distanceToPin <= 500 && distanceToPin > 100)
            {
                Toast.MakeText(this, "WARNING: Low Bridge within 500m", ToastLength.Long).Show();
            }
            else if (distanceToPin <= 1000 && distanceToPin > 500)
            {
                Toast.MakeText(this, "CAUTION: Low Bridge. Distance to Bridge " + (int)distanceToPin + " meters", ToastLength.Long).Show();
                vibrator.Vibrate(500);
            }
			else if (distanceToPin > 20000)
			{
				Toast.MakeText(this, "NOTICE: You are outside the supported area", ToastLength.Long).Show();
			}
        }

        private void MoveToLocation(GeoLocation geoLocation, float zoom = 18)
        {
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(new LatLng(geoLocation.Latitude, geoLocation.Longitude));

            builder.Zoom(zoom);
            var cameraPosition = builder.Build();
            var cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);

            mMap.MoveCamera(cameraUpdate);
        }

        //When coming back from another screen we want to see if the user has selected 
        //a vehicle. Pull the data from DataHolder and update the local view
        protected override void OnResume()
        {
            base.OnResume();

            TextView vehicleText = (TextView)FindViewById(Resource.Id.txtVehicle);

            if (mapVm.CurrentVehicle != null)
            {
                vehicleText.Text = mapVm.CurrentVehicle.Registration;
                UpdateAllBridges();
                FindNearestBridge();
            } else
            {
                vehicleText.Text = "No vehicle selected";
            }
        }
    }
}
