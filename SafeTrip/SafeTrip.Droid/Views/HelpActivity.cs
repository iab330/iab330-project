using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using MvvmCross.Droid.Views;

namespace SafeTrip.Droid.Views
{
    [Activity(Label = "@string/helpActivity")]
    public class HelpActivity : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.HelpView);

            //Custom (centered text) Actionbar
            ActionBar.SetDisplayShowCustomEnabled(true);
            ActionBar.SetCustomView(Resource.Layout.centeredActionbar);
            TextView titleText = (TextView)FindViewById(Resource.Id.titleText);
            titleText.SetText(Resource.String.helpActivity);

            // Get our UI controls from the loaded layout
            Button emergencyButton = FindViewById<Button>(Resource.Id.emergencyButton);

            emergencyButton.Click += (sender, e) =>
            {
                // On "Call" button click, try to dial phone number.
                var callDialog = new AlertDialog.Builder(this);
                callDialog.SetMessage("Call 000?");
                callDialog.SetNeutralButton("Call", delegate {
                    // Create intent to dial phone
                    var callIntent = new Intent(Intent.ActionCall);
                    callIntent.SetData(Android.Net.Uri.Parse("tel:000"));
                    StartActivity(callIntent);
                });
                callDialog.SetNegativeButton("Cancel", delegate { });

                // Show the alert dialog to the user and wait for response.
                callDialog.Show();
            };
        }
    }
}
