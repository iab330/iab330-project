﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SQLite.Net;
using SafeTrip.Core.Interfaces;
using SafeTrip.Core.Models;
using MvvmCross.Platform;
using System.Threading.Tasks;

namespace SafeTrip.Core.Database
{
    public class VehicleDatabase : IVehicleDatabase
    {
        private SQLiteConnection database;
        public VehicleDatabase()
        {
            var sqlite = Mvx.Resolve<ISqlite>();
            database = sqlite.GetConnection();
            database.CreateTable<Vehicle>();
        }

        public async Task<IEnumerable<Vehicle>> GetVehicles()
        {
            return database.Table<Vehicle>().ToList();
        }

        public async Task<int> InsertVehicle(Vehicle vehicle)
        {
            var num = database.Insert(vehicle);
            database.Commit();
            return num;
        }

        public async Task<int> DeleteVehicle(object id)
        {
            return database.Delete<Vehicle>(Convert.ToInt16(id));
        }

        public async Task<bool> CheckIfExists(Vehicle location)
        {
            var exists = database.Table<Vehicle>()
                .Any(x => x.Registration == location.Registration);
            return exists;
        }
    }
}