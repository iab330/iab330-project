﻿using SafeTrip.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeTrip.Core.Interfaces
{
    public interface IVehicleDatabase
    {
        Task<IEnumerable<Vehicle>> GetVehicles();

        Task<int> DeleteVehicle(object id);

        Task<int> InsertVehicle(Vehicle vehicle);

        Task<bool> CheckIfExists(Vehicle vehicle);
    }
}
