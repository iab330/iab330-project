﻿using SQLite.Net;

namespace SafeTrip.Core.Interfaces
{
    public interface ISqlite
    {
        SQLiteConnection GetConnection();
    }
}
