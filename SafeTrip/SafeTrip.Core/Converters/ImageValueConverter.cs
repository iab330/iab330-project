﻿using MvvmCross.Platform.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeTrip.Core
{
    public class ImageValueConverter : MvxValueConverter<bool, string>
    {
        protected override string Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value)
            {
                return "res:Icon";
            }
            else
            {
                return "res:map_placeholder";
            }
        }
    }
}
