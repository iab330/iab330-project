﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeTrip.Core.Models
{
    public class Bridge
    {
        public string ASSET_ID { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
		public double Signed_Clearance { get; set; }
        public string direction { get; set; }
        public string suburb { get; set; }
        public string Street_Name { get; set; }
        public string description { get; set; }

        public Bridge() { }
        public Bridge(string ASSET_ID, double latitude, double longitude, double Signed_Clearance, string direction, string suburb, string Street_Name, string description)
        {
            this.ASSET_ID = ASSET_ID;
            this.latitude = latitude;
            this.longitude = longitude;
			this.Signed_Clearance = Signed_Clearance;
            this.direction = direction;
            this.suburb = suburb;
            this.Street_Name = Street_Name;
            this.description = description;
        }
    }
}
