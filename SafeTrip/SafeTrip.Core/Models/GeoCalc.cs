﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeTrip.Core.Models
{
    public static class GeoCalc
    {
        const double eQuatorialEarthRadius = 6378.1370D;
        const double d2r = (Math.PI / 180D);

        public static double GetDistance(Bridge bridge, double myLat, double myLong, bool metres = true)
        {
            if (metres)
                return distanceInM(myLat, myLong, bridge.latitude, bridge.longitude);
            else
                return distanceInKM(myLat, myLong, bridge.latitude, bridge.longitude);
        }

        public static double GetDistance(double origLat, double origLong, double newLat, double newLong, bool metres = true)
        {
            if (metres)
                return distanceInM(origLat, origLong, newLat, newLong);
            else
                return distanceInKM(origLat, origLong, newLat, newLong);
        }

        private static double distanceInM(double lat1, double long1, double lat2, double long2)
        {
            return (1000D * distanceInKM(lat1, long1, lat2, long2));
        }

        private static double distanceInKM(double lat1, double long1, double lat2, double long2)
        {
            double dlong = (long2 - long1) * d2r;
            double dlat = (lat2 - lat1) * d2r;
            double a = Math.Pow(Math.Sin(dlat / 2D), 2D) + Math.Cos(lat1 * d2r) * Math.Cos(lat2 * d2r) * Math.Pow(Math.Sin(dlong / 2D), 2D);
            double c = 2D * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1D - a));
            double d = eQuatorialEarthRadius * c;

            return d;
        }
    }
}
