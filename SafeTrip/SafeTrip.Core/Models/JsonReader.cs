﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SafeTrip.Core.Models
{
    public static class JsonReader
    {
        public static List<Bridge> DeserializeBridgeJson(string json)
        {
            //string modifiedJson = @json;
            JsonSerializerSettings jsonSettings = new JsonSerializerSettings();
            jsonSettings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Bridge> bridges = JsonConvert.DeserializeObject<List<Bridge>>(json, jsonSettings);
            return bridges;
        }
        
    }
}
