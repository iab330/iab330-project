﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace SafeTrip.Core.Models
{
    public class Vehicle
    {
        private bool isItemSelected = false;

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Nickname { get; set; }
        public double Height { get; set; }
        public double extraHeight { get; set; }
        public double Length { get; set; }
        public string Registration { get; set; }
        //[Ignore]
        //will be enum later on
        //public string Type { get; set; }
        [Ignore]
        public bool IsItemSelected {
            get { return isItemSelected; }
            set { isItemSelected = value; }
        }
        public double totalHeight()
        {
            return Height + extraHeight;
        }
        public Vehicle() { }
        public Vehicle(string nickname, double height, double length, string registration)//, string type)
        {
            Nickname = nickname;
            Height = height;
            Length = length;
            Registration = registration;
            //Type = type;
        }

        public void EditVehicle(string nickname, double height, double length, string registration)//, string type)
        {
            Nickname = nickname;
            Height = height;
            Length = length;
            Registration = registration;
            //Type = type;
        }

        public bool CompareCanPass(double height)
        {
            return (totalHeight() <= height);
        }
    }
}
