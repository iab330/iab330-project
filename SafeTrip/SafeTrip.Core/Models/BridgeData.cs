﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeTrip.Core.Models
{
    public class BridgeData
    {
        private List<Bridge> bridgeList;
        //__OPTIMIZATION__: we don't technically need nearList atm, but it could be useful later
        private List<Bridge> nearList;

        public BridgeData()
        {
            nearList = new List<Bridge>();
            bridgeList = new List<Bridge>();
        }

        public BridgeData(string json):base()
        {
            ReadAndSetupBridges(json);
        }

        public List<Bridge> NearList
        {
            get { return nearList; }
        }

        public List<Bridge> BridgeList
        {
            get { return bridgeList; }
        }

        public void ReadAndSetupBridges(string json)
        {
            bridgeList = JsonReader.DeserializeBridgeJson(json);
        }

        public List<Bridge> CheckWithinDistance(double latitude, double longitude, double distThresholdM)
        {
            List<Bridge> nearBridges = new List<Bridge>();
            CheckBridgesAndAdd(latitude, longitude, distThresholdM, nearBridges);
            nearList = nearBridges;
            return nearBridges;
        }

        public Tuple<Bridge, double> GetNearest(double latitude, double longitude)
        {
            if (bridgeList.Count > 0)
            {
                Bridge nearestBridge = bridgeList[0];
                double nearestDist = GeoCalc.GetDistance(bridgeList[0], latitude, longitude);
                foreach (Bridge bridge in bridgeList)
                {
                    double dist = GeoCalc.GetDistance(bridge, latitude, longitude);
                    if (dist < nearestDist)
                    {
                        nearestBridge = bridge;
                        nearestDist = dist;
                    }
                }
                return Tuple.Create(nearestBridge, nearestDist);
            }
            else return null;
        }

        public Tuple<Bridge, double> GetNearest(double latitude, double longitude, List<Bridge> bridges)
        {
            if (bridges.Count > 0)
            {
                Bridge NearestBridge = bridges[0];
                double nearestDist = GeoCalc.GetDistance(bridges[0], latitude, longitude);
                foreach (Bridge bridge in bridges)
                {
                    double dist = GeoCalc.GetDistance(bridge, latitude, longitude);
                    if (dist < nearestDist)
                    {
                        NearestBridge = bridge;
                        nearestDist = dist;
                    }
                }
                return Tuple.Create(NearestBridge,nearestDist);
            }
            else return null;
        }

        public Bridge GetNearestWithinDistance(double latitude, double longitude, double distThresholdM)
        {
            List<Bridge> nears = CheckWithinDistance(latitude, longitude, distThresholdM);
            if (nears != null)
            {
                return GetNearest(latitude, longitude, nears).Item1;
            }
            else return null; 
        }

        private void CheckBridgesAndAdd(double latitude, double longitude, double distThresholdM, List<Bridge> nearBridges)
        {
            foreach (Bridge bridge in bridgeList)
            {
                if (GeoCalc.GetDistance(bridge, latitude, longitude) <= distThresholdM)
                {
                    nearBridges.Add(bridge);
                }
            }
        }
    }
}
