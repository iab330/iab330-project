using MvvmCross.Core.ViewModels;
using SafeTrip.Core.Interfaces;
using SafeTrip.Core.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace SafeTrip.Core.ViewModels
{
    public class AddVehicleViewModel : MvxViewModel
    {
        private ObservableCollection<Vehicle> vehicles;
        private readonly IDialogService dialog;
        private readonly IVehicleDatabase vehiclesDatabase;

        public ICommand CommitCommand { get; private set; }

        public ObservableCollection<Vehicle> Vehicles {
            get { return vehicles; }
            set { SetProperty(ref vehicles, value); }
        }

        private string inputNickname;
        private double inputHeight;
        private double inputLength;
        private string inputRegistration;

        public string InputNickname {
            get { return inputNickname; }
            set { SetProperty(ref inputNickname, value); }
        }
        public double InputHeight {
            get { return inputHeight; }
            set { SetProperty(ref inputHeight, value); }
        }

        public double InputLength {
            get { return inputLength; }
            set { SetProperty(ref inputLength, value); }
        }

        public string InputRegistration {
            get { return inputRegistration; }
            set { SetProperty(ref inputRegistration, value); }
        }


        public AddVehicleViewModel(IDialogService dialog, IVehicleDatabase vehiclesDatabase)
        {
            this.dialog = dialog;
            this.vehiclesDatabase = vehiclesDatabase;
            Vehicles = new ObservableCollection<Vehicle>();
            CommitCommand = new MvxCommand<Vehicle>(selectedVehicle =>
            {
                SelectVehicle(selectedVehicle = new Vehicle(inputNickname, inputHeight, InputLength, inputRegistration));
            });
        }

        public async void SelectVehicle(Vehicle selectedVehicle)
        {
            if (!await vehiclesDatabase.CheckIfExists(selectedVehicle))
            {
                await vehiclesDatabase.InsertVehicle(selectedVehicle);

                Close(this);
            }
            else if (!await dialog.Show("This vehicle has already been added", "Vehicle Exists", "Change", "Discard"))
            {
                Close(this);
            }
        }
    }
}
