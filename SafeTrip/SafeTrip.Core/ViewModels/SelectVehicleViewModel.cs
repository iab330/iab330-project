using MvvmCross.Core.ViewModels;
using SafeTrip.Core.Interfaces;
using SafeTrip.Core.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;
using SafeTrip.Core.ViewModels;
using System.ComponentModel;
using System.Threading.Tasks;
using SafeTrip.Core.Database;

namespace SafeTrip.Core.ViewModels
{
    public class SelectVehicleViewModel : MvxViewModel
    {
        //Class global variables
        private ObservableCollection<Vehicle> vehicles = new ObservableCollection<Vehicle>();
        private readonly IDialogService dialog;
        private readonly IVehicleDatabase vehiclesDatabase;
        private Vehicle myChosenVehicle;

        //Commands (buttons, tap list etc)
        public ICommand AddVehicleCommand { get; private set; }
        public ICommand DeleteVehicleCommand { get; private set; }
        public ICommand SelectVehicleCommand { get; private set; }

        public ObservableCollection<Vehicle> Vehicles {
            get { return vehicles; }
            set { SetProperty(ref vehicles, value); }
        }

        public Vehicle MyChosenVehicle {
            get { return myChosenVehicle; }
            set {
                if (myChosenVehicle != value)
                {
                    myChosenVehicle = value;
                    DataHolder.setData(myChosenVehicle);
                    RaisePropertyChanged("MyChosenVehicle");
                }
            }
        }

        public SelectVehicleViewModel(IDialogService dialog, IVehicleDatabase vehiclesDatabase)
        {
            this.dialog = dialog;
            this.vehiclesDatabase = vehiclesDatabase;
            AddVehicleCommand = new MvxCommand(() => ShowViewModel<AddVehicleViewModel>());
            DeleteVehicleCommand = new MvxCommand(DeleteVehicle);
            SelectVehicleCommand = new MvxCommand(OnItemSelected);
        }

        private async void OnItemSelected()
        {       
            if (await dialog.Show(string.Format("You are now driving {0}", myChosenVehicle.Nickname), "Vehicle Selected", "OK"))
            {
                UpdateItemSelections();
                Close(this);
            }
        }

        private void DeleteVehicle()
        {
            if (MyChosenVehicle != null)
            {
                vehiclesDatabase.DeleteVehicle(MyChosenVehicle.ID);
                MyChosenVehicle = null;
                GetVehicles();
            }

        }

        private void UpdateItemSelections()
        {
            if (vehicles.Count > 0)
            {
                for (int index = 0; index < vehicles.Count; index++)
                {
                    // If the chosen item is the same, mark it as selected
                    if (vehicles[index].Registration.Equals(MyChosenVehicle.Registration))
                    {
                        // Toggle selected status
                        vehicles[index].IsItemSelected = !vehicles[index].IsItemSelected;
                    }
                    else
                    {
                        // Only trigger the property changed event if it needs to change
                        if (vehicles[index].IsItemSelected)
                        {
                            vehicles[index].IsItemSelected = false;
                        }
                    }
                }
            }
        }

        public void OnResume()
        {
            GetVehicles();
        }

        public async void GetVehicles()
        {
            var vehiclesList = await vehiclesDatabase.GetVehicles();
            vehicles.Clear();
            foreach (var vehicles in vehiclesList)
            {
                if (vehicles != null)
                {
                    Vehicles.Add(vehicles);
                }
            }
        }
    }
}