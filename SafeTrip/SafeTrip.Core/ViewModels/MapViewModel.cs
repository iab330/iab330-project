using MvvmCross.Core.ViewModels;
using SafeTrip.Core.Interfaces;
using SafeTrip.Core.Models;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace SafeTrip.Core.ViewModels
{
    public class MapViewModel : MvxViewModel
    {

        private BridgeData bridges = new BridgeData();
        private GeoLocation myLocation;
        private IGeoCoder geocoder;
        private Action<GeoLocation, float> moveToLocation;
        private readonly IDialogService dialog;
        //private Vehicle currentVehicle;

        //Viewmodel commands
        public ICommand OpenVehiclesCommand { get; private set; }
        public ICommand OpenHelpCommand { get; private set; }
	
	    public BridgeData Bridges
        {
            get { return bridges; }
        }

        public Vehicle CurrentVehicle
        {
            get { return DataHolder.getData(); }
        }

        private ObservableCollection<EddyStone> eddyStoneList = new ObservableCollection<EddyStone>();
        public ObservableCollection<EddyStone> EddyStoneList {
            get { return eddyStoneList; }
            set { SetProperty(ref eddyStoneList, value); }
        }

        public GeoLocation MyLocation
        {
            get { return myLocation; }
            set { myLocation = value; }
        }

        public MapViewModel(IDialogService dialog, IGeoCoder geocoder)
        {
            this.dialog = dialog;
            this.geocoder = geocoder;
            OpenVehiclesCommand = new MvxCommand(() => ShowViewModel<SelectVehicleViewModel>());
            OpenHelpCommand = new MvxCommand(() => ShowViewModel<HelpViewModel>());
        }

        public void OnMyLocationChanged(GeoLocation location)
        {
            MyLocation = location;
        }

        public void OnMapSetup(Action<GeoLocation, float> MoveToLocation)
        {
            moveToLocation = MoveToLocation;
        }

        public void BeaconInRange()
        {
            Vehicle veh = new Vehicle("haha", 1, 1, "609JKL");

            if (DataHolder.getData() == null)
            {
                if (EddyStoneList.Count > 0)
                {
                    if (EddyStoneList[0].MacAddress.ToString() != null)
                    {
                        dialog.Show(string.Format("Automatically driving {0}", veh.Registration), "iBeacon Detected");
                        DataHolder.setData(veh);
                    }
                }
            }
        }

        //public void OnResume()
        //{
        //    currentVehicle = DataHolder.getData();
        //}
    }

    public class DataHolder
    {
        private static Vehicle data;
        public static Vehicle getData() { return data; }
        public static void setData(Vehicle data) { DataHolder.data = data; }
    }
}
