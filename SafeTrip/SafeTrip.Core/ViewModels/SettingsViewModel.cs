using MvvmCross.Core.ViewModels;

namespace SafeTrip.Core.ViewModels
{
    public class SettingsViewModel 
        : MvxViewModel
    {
        private string _hello = "Hello MvvmCross";
        public string Hello
        { 
            get { return _hello; }
            set { SetProperty (ref _hello, value); }
        }
    }
}
